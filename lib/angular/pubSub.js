class PubSub {
  constructor() {
    this.events = [];
  }

  subscribe(name, listener) {
    if (!this.events[name]) {
      this.events[name] = [];
    }

    this.events[name].push(listener);
  }

  publish(name, data) {
    const event = this.events[name];

    if (!event || !event.length) {
      return;
    }

    this.events.forEach(el => el(data));
  }
}

export default new PubSub();
