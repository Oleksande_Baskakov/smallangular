function ngRepeat() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-repeat'];
      const [variable, model] = value.split(' in ');
      const { parentNode } = el;
      const { display } = scope.getComputedStyle(el);
      scope[variable] = '';

      if (!scope.display) {
        scope.display = display;
      }

      const update = () => {
        for (let i = 1; i < parentNode.children.length;) {
          parentNode.children[i].remove();
        }

        for (const item of scope[model]) {
          const elCopy = el.cloneNode(true);
          scope[variable] = item;

          const regtoRemoveDirrect = new RegExp(`ng-repeat=["']${value}["']`);
          const regExpForCondition = new RegExp(`ng-if=["']${variable}`);
          const elCopyHtml = elCopy.outerHTML
            .replace(/{{(.*)}}/g, (match, p1) => scope.$$parse(p1))
            .replace(regtoRemoveDirrect, '')
            .replace(/display: none/, `display: ${scope.display}`)
            .replace(regExpForCondition, `ng-if="${Number(item) ? item : `'${item}'`}`);
          parentNode.insertAdjacentHTML('beforeend', elCopyHtml);
          scope.$$compile(parentNode.lastChild);
        }
      };
      el.style.display = 'none';
      update();
      scope.$whatch('ngRepeat', el, update);
    }
  };
}

export default ngRepeat;
