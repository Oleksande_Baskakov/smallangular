function ngInit() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-init'];
      eval.call(scope, value);
    }
  };
}

export default ngInit;
