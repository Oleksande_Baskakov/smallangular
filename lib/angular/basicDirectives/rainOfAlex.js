import '../assets/css/rainOfAlex.css';

function rainOfAlex() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['rain_of_alex'];

      for (let i = 0; i < Number(value); i++) {
        const div = document.createElement('div');
        div.setAttribute('class', 'snow');
        el.append(div);
      }
    }
  };
}

export default rainOfAlex;
