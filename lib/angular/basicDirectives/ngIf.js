function ngIf() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-if'];
      const { display } = window.getComputedStyle(el);

      const update = () => {
        el.style.display = eval(value) ? display : 'none';
      };

      update();
      scope.$whatch('ngIf', el, update);
    }
  };
}

export default ngIf;
