function ngModel() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-model'];
      scope[value] = '';

      const update = () => {
        el.checked = scope[value];
      };

      el.addEventListener('input', e => {
        scope[value] = el.type === 'text' ? e.target.value : e.target.checked;
        scope.$applyAsync();
      });

      update();
      scope.$whatch('ngModel', el, update);
    }
  };
}

export default ngModel;
