function ngBind() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-bind'];

      const update = () => {
        el.innerText = `${eval(value)}`;
      };

      update();
      scope.$whatch('ngBind', el, update);
    }
  };
}

export default ngBind;
