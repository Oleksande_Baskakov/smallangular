function ngShow() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-show'];
      const update = () => {
        el.hidden = !eval(value);
      };

      update();
      scope.$whatch('ngShow', el, update);
    }
  };
}

export default ngShow;
