function ngClick() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-click'];
      el.addEventListener('click', function() {
        eval(value);
      });
    }
  };
}

export default ngClick;
