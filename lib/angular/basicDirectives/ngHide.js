function ngHide() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-hide'];

      const update = () => {
        el.hidden = eval(value);
      };

      update();
      scope.$whatch('ngHide', el, update);
    }
  };
}

export default ngHide;
