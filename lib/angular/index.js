import * as baseFilters from './filters';
import * as basicDirectives from './basicDirectives';
import pubSub from './pubSub';
import { toCamelCase, filterNgAttrs, parseDependencies } from './utils';
import { timeout, http } from './services';

function SmallAngular() {
  let watchers = [];
  const directives = {};
  const components = {};
  const controllers = {};
  const configs = [];
  const services = {};
  const filters = { ...baseFilters };
  const $rootScope = window;

  // eslint-disable-next-line no-proto
  Object.assign($rootScope.__proto__, pubSub);
  Object.assign(directives, basicDirectives);

  function runConfigFunctions() {
    configs.forEach(el => el());
  }

  const searchDependecies = args => {
    const dependencies = args instanceof Function ? parseDependencies(args) : args;

    return dependencies.map(el => {
      if (el === '$rootScope') {
        return $rootScope;
      }

      if (!services[el]) {
        throw new Error('the service you ask for in dependencies does not exist in the list of services');
      }

      return services[el];
    });
  };

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  function component(name, fn) {
    components[name] = fn;

    return this;
  }

  function controller(name, fn) {
    controllers[name] = fn;

    return this;
  }

  function filter(name, fn) {
    filters[name] = fn;

    return this;
  }

  function service(name, args) {
    const fn = typeof args === 'function' ? args : args.pop();
    const dependencies = searchDependecies(args);
    services[name] = fn(...dependencies);

    return this;
  }

  function constant(name, value) {
    services[name] = value;
  }

  function config(fn) {
    const dependencies = searchDependecies(fn);
    configs.push(fn.bind({}, ...dependencies));
  }

  function parse(expression) {
    const [variable, fn] = expression.split(/\s*\|\s*/);
    const variableValue = eval(variable);

    return (fn && filters[fn]) ? filters[fn](variableValue) : variableValue;
  }

  function compile(node) {
    const component = components[toCamelCase(node.tagName)];
    const filteredAttrs = filterNgAttrs(node.attributes, directives);

    if (component) {
      const { controllerAss, controller, template, link } = component();
      const controllerInstance = new controllers[controller]();

      if (controllerAss) {
        $rootScope[controllerAss] = controllerInstance;
      }

      if (template) {
        node.innerHTML = template;
      }

      link($rootScope, node, filteredAttrs, controllerInstance);
    }

    for (const key of node.getAttributeNames()) {
      const camelCaseKey = toCamelCase(key);

      if (camelCaseKey in directives) {
        directives[camelCaseKey]().link($rootScope, node, filteredAttrs);
      }
    }
  }


  $rootScope.$apply = () => {
    const watchersForConnectedNodes = [];

    for (const obj of watchers) {
      if (obj.node.isConnected) {
        obj.callback();
        watchersForConnectedNodes.push(obj);
      }
    }
    watchers = watchersForConnectedNodes;
  };

  $rootScope.$applyAsync = () => {
    setTimeout($rootScope.$apply);
  };

  $rootScope.$whatch = (name, node, callback) => {
    watchers.push({ node, callback });
  };

  this.createApp = appName => {
    this.appName = appName;
    return this;
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    const collection = node.querySelectorAll('*');
    runConfigFunctions();
    collection.forEach(compile);
  };

  $rootScope.$$parse = parse;
  $rootScope.$$compile = compile;

  this.directive = directive;
  this.controller = controller;
  this.service = service;
  this.component = component;
  this.constant = constant;
  this.config = config;
  this.filter = filter;

  service('timeout', timeout);
  service('http', http);
  setTimeout(this.bootstrap);
}

window.angular = new SmallAngular();
