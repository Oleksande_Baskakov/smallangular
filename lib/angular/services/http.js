function http($rootScope) {
  'inject';

  function request(config) {
    const {
      method,
      body,
      url,
      params,
      transformResponse,
      onUploadProgress,
      onDownloadProgress,
      responseType = 'json'
    } = config;
    let fullUrl = url;

    const xhr = new XMLHttpRequest();

    if (params) {
      fullUrl += `${fullUrl.includes('?') ? '&' : '?'}${new URLSearchParams(params)}`;
    }

    xhr.open(method, fullUrl);
    xhr.responseType = responseType;
    const headers = {
      ...config.headers
    };

    for (const key in headers) {
      xhr.setRequestHeader(key, headers[key]);
    }

    xhr.upload.onprogress = onUploadProgress;
    xhr.onprogress = onDownloadProgress;
    xhr.send(body || null);

    return new Promise((res, rej) => {
      xhr.addEventListener('error', rej);
      xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState !== 4) {
          $rootScope.$applyAsync();
          return;
        }

        const contentType = xhr.getResponseHeader('Content-Type');
        const response = {
          status: xhr.status,
          statusText: xhr.statusText,
          url: xhr.responseURL,
          data: xhr.response,
          responseType: xhr.responseType,
          contentType
        };

        if (transformResponse) {
          response.data = transformResponse.reduce((accum, el) => el(accum), response.data);
        }

        $rootScope.$applyAsync();
        res(response);
      });
    });
  }

  return {
    get: (url, config = {}) => request({ ...config, method: 'GET', url }),
    post: (url, data, config = {}) => request({ ...config, body: data, method: 'POST', url }),
    delete: (url, config = {}) => request({ ...config, method: 'DELETE', url })
  };
}

export default http;
