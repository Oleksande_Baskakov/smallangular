function timeout($rootScope) {
  'inject';

  return function(fn, delay) {
    setTimeout(() => {
      fn();
      $rootScope.$applyAsync();
    }, delay);
  };
}

export default timeout;
