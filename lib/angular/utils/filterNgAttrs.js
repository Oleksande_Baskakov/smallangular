const filterNgAttrs = (attrs, directives) => {
  const result = {};

  for (let i = 0; i < attrs.length; i++) {
    if (directives[attrs[i].name]) {
      continue;
    }
    result[attrs[i].name] = attrs[i].value;
  }

  return result;
};

export default filterNgAttrs;
