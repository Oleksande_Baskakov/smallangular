const parseDependencies = fn => {
  const fnStr = fn.toString();

  if (!(/{\n\s+('inject')/).test(fnStr)) {
    return [];
  }

  const [, argsStr] = fnStr.match(/\((.*?)\)/);

  return argsStr.split(/\s*,\s*/);
};

export default parseDependencies;
